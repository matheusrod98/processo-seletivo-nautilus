class Team:

    def __init__ (self, name, wins, ties, losses, goals_for, goals_against):

        self.name = name
        self.wins = wins
        self.losses = losses
        self.ties = ties
        self.points = (self.wins * 3) + (self.ties)
        self.goals_for = goals_for
        self.goals_against = goals_against
        self.goals_difference = goals_for - goals_against


    def __repr__ (self):
        stats = ("Name: {}\nPoints: {}\nWins: {}\nLosses: {}\nTies: {}\nGoals for: {}\nGoals Against: {}\nGoals difference: {}."
        .format (self.name, self.points, self.wins, self.losses, self.ties, self.goals_for, self.goals_against, self.goals_difference))

        return stats

    def __add__ (self, other_team):
        res = self.points + other_team.points
        return res

    def __sub__ (self, other_team):
        res = self.points - other_team.points
        return res

    def __gt__ (self, other_team):
        return (self.points > other_team.points)

def create_table (team_list):

    sorted_team_list = sorted (team_list, key = lambda x: x.points, reverse = True)
   
    print ("")
    print ("\t\tTabela do Campeonato Carioca 2022")
    print ("")
    print ("{:<16} {:<8} {:<8} {:<8} {:<8} {:<8} {:<8} {:<8}" .format ("Team", "Pts", "W", "L", "T", "GF", "GA", "GD"))

    for team in sorted_team_list:
        print ("{:<16} {:<8} {:<8} {:<8} {:<8} {:<8} {:<8} {:<8}" 
                .format (team.name, team.points, team.wins, team.losses, team.ties, team.goals_for, team.goals_against, team.goals_difference))
    print ("")

def main ():

    flamengo = Team ("Flamengo", 3, 0, 2, 7, 5)
    fluminense = Team ("Fluminense", 3, 1, 3, 7, 9)
    vasco = Team ("Vasco", 3, 1, 4, 9, 10)
    botafogo = Team ("Botafogo", 3, 3, 2, 10, 6)
    america = Team ("América", 6, 3, 2, 12, 6)

    team_list = [flamengo, fluminense, vasco, botafogo, america]

    print ("Bem vindo ao Campeonato Carioca 2022, temos os seguintes times competindo: ")
    for team in team_list:
        print ("{}" .format (team.name))
    print ("")

    print ("Escolha uma opção: ")
    print ("1 - Printar informações de uma das equipes.")
    print ("2 - Printar a tabela atual do campeonato. (Ordenada por pontos)")
    print ("3 - Printar a soma de pontos de dois times.")
    print ("4 - Printar a subtração de pontos de dois times.")
    print ("5 - Comparar dois times e descobrir qual tem mais pontos. (__gt__)")
    option = int (input (">> "))

    if (option == 1):
        team_name = input ("Entre com o nome do time: ")
        
        team_names = []
        for team in team_list:
            team_names.append (team.name)

        if team_name in team_names:
            index = team_names.index (team_name)
            print ("")
            print (team_list [index])
            print ("")

        else:
            print ("Time não encontrado, tente novamente.")
            return -1

    elif (option == 2):
        create_table (team_list)

    elif (option == 3):
        teamA = input ("Entre com o primeiro time da soma: ")
        teamB = input ("Entre com o segundo time da soma: ")

        team_names = []
        for team in team_list:
            team_names.append (team.name)

        if teamA in team_names and teamB in team_names:
            indexA = team_names.index (teamA)
            indexB = team_names.index (teamB)
            print ("")
            print ("A soma dos pontos de {} com os pontos de {} é: {}" .format (teamA, teamB, team_list [indexA] + team_list [indexB]))
            print ("")

        else:
            print ("Um ou mais times não existem, tente novamente.")
            return -1

    elif (option == 4):
        teamA = input ("Entre com o primeiro time da subtração: ")
        teamB = input ("Entre com o segundo time da subtração: ")

        team_names = []
        for team in team_list:
            team_names.append (team.name)

        if teamA in team_names and teamB in team_names:
            indexA = team_names.index (teamA)
            indexB = team_names.index (teamB)
            print ("")
            print ("A subtração dos pontos de {} com os pontos de {} é: {}" .format (teamA, teamB, team_list [indexA] - team_list [indexB]))
            print ("")

    elif (option == 5):

        teamA = input ("Entre com o primeiro time da comparação: ")
        teamB = input ("Entre com o segundo time da comparação: ")

        team_names = []
        for team in team_list:
            team_names.append (team.name)

        if teamA in team_names and teamB in team_names:
            indexA = team_names.index (teamA)
            indexB = team_names.index (teamB)

            if (team_list [indexA] > team_list [indexB]):
                print ("")
                print ("O time {} tem mais pontos que o time {}." .format (teamA, teamB))
                print ("")

            else:

                print ("")
                print ("O {} não tem mais pontos que o {}." .format (teamA, teamB))
                print ("")

        else:
            print ("Um ou mais times não existem, tente novamente.")
            return -1

    else: 
        print ("Opção inválida, tente novamente.")
        
    return 0

main ()
