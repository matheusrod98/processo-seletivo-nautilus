#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH='/home/matheus/Desktop/Processo Seletivo - Nautilus/ROS - Básico/Example_Workspace/devel:/opt/ros/noetic'
export LD_LIBRARY_PATH='/opt/ros/noetic/lib'
export PWD='/home/matheus/Desktop/Processo Seletivo - Nautilus/ROS - Básico/Example_Workspace/build'
export ROSLISP_PACKAGE_DIRECTORIES='/home/matheus/Desktop/Processo Seletivo - Nautilus/ROS - Básico/Example_Workspace/devel/share/common-lisp'
export ROS_PACKAGE_PATH='/home/matheus/Desktop/Processo Seletivo - Nautilus/ROS - Básico/Example_Workspace/src:/opt/ros/noetic/share'