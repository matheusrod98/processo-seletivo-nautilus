#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float32
from geometry_msgs.msg import Point

class DistanceCalculator ():

    def __init__ (self):
        rospy.init_node ('distance_calculator', anonymous = True)
        self.pub = rospy.Publisher ('distance', Float32, queue_size = 10)
        self.sub = rospy.Subscriber ('position', Point, self.callback) 
        rospy.spin ()

    def callback (self, msg):
        x = msg.x
        y = msg.y
        z = msg.z

        distance = (x ** 2 + y ** 2 + z ** 2) ** 0.5
        f = Float32 ()
        f.data = distance
        self.pub.publish (f)

if __name__ == '__main__':
    try:
        d = DistanceCalculator ()
    except rospy.ROSInterruptException:
        pass
