#!/usr/bin/env python3

import rospy
from random import randint
from geometry_msgs.msg import Point

class PositionSensor ():

    def __init__ (self):
        rospy.init_node ('position_sensor', anonymous = True)
        self.pub = rospy.Publisher ('position', Point, queue_size = 10)

    def start (self):
        
        rate = rospy.Rate (10)
        while not rospy.is_shutdown ():
            p = Point ()
            p.x = randint (-10, 10)
            p.y = randint (-10, 10)
            p.z = randint (-10, 10)
            self.pub.publish (p)

if __name__ == '__main__':
    try:
        s = PositionSensor ()
        s.start ()
    except rospy.ROSInterruptException:
        pass
