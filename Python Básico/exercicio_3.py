def find_ways (target):

    ways = 0

    for a in range (1 + int (target / 100)):
        for b in range (1 + int ((target - 100 * a) / 50)):
            for c in range (1 + int ((target - 100 * a - 50 * b) / 20)):
                for d in range (1 + int ((target - 100 * a - 50 * b - 20 * c) / 10)):
                    for e in range (1 + int ((target - 100 * a -  50 * b - 20 * c - 10 * d) / 5)):
                        for f in range (1 + int ((target - 100 * a - 50 * b - 20 * c - 10 * d - 5 * e) / 2)):
                            ways += 1
    ways += 1

    return ways

def main ():

    target = 200
    res = find_ways (target)
    print ("We can make {}p in {} ways." .format (target, res))

    return 0

main ()
