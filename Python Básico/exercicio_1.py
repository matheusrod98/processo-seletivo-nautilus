def generate_sequence (initial_value, limit, common_difference):

    sequence = [initial_value]

    # If common_difference is an even value, the function will generate an arithmetic progression.
    if (common_difference % 2 == 0):

        # This handles the negative sequence case.
        if (common_difference < 0):
            while (initial_value >= (limit - common_difference)):
                initial_value += common_difference
                sequence.append (initial_value)

        else:
            while (initial_value <= (limit - common_difference)):
                initial_value += common_difference
                sequence.append (initial_value)

    # If common_difference is an odd value, the function will generate an geometric progression.
    else:

        # This handles the alternating series case.
        if (common_difference < 0 or initial_value < 0):
            while (initial_value >= (limit / common_difference)):
                initial_value *= common_difference
                sequence.append (initial_value)

        else:
            while (initial_value <= (limit / common_difference)):
                initial_value *= common_difference
                sequence.append (initial_value)

    return sequence

def main ():

    initial_value = input ("Sequence generator started, please, first, enter the initial value of the sequence: ")
    limit = input ("Enter the limit of the sequence: ")
    common_difference = input ("Enter the common difference between the sequence terms: ")

    if (common_difference == '' or limit == '' or initial_value == ''):
        print ("Empty values are not allowed. Please try again with the appropriate values.")
        return -1

    elif (int (common_difference) == 0 or int (common_difference) == 1):
        print ("The common difference can't be zero or one. Please try again with the appropriate values.")
        return -1

    elif ((int (limit) * int (common_difference)) < 0 and (int (common_difference) % 2 == 0)):
        print ("The sign of common difference and limit can't be different for arithmetic progression. Please try again with the appropriate values.")
        return -1

    elif ((int (limit) < 0 and (int (common_difference) % 2 != 0))):
        print ("Please, pick a positive limit for alternating geometric progression.")
        return -1

    sequence = generate_sequence (int (initial_value), int (limit), int (common_difference))

    print (sequence)

    return 0

main ()
