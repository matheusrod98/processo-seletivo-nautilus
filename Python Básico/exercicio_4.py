def find_last_digits ():

    res = 0
    index = 1
    n = 1000

    for index in range (n + 1):
        res += index ** index

    return str (res) [-11:]

def main ():

    print ("The last 10 digits of the series are: {}." .format (find_last_digits ()))
    
    return 0

main ()
