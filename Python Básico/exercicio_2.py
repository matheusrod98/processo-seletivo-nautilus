def find_one_bits (dividend):

    one_bits = 0
    while (dividend >= 1):

        if (dividend % 2 == 1):
            one_bits += 1

        dividend = dividend // 2

    return one_bits

def main ():

    dividend = int (input ("Please enter a integer number in the decimal form: "))
    res = find_one_bits (dividend)
    print ("The number {} has {} one bits in the binary form." .format (dividend, res))
    
    return 0

main ()
