# Exercício 1

Para executar o programa:  `python3 exercicio_1.py`

## Observações
- Razão igual a 0 ou 1 não são permitidas, não gerariam uma saída de acordo com o que foi pedido.
- A série geométrica alternada só será executada com a inserção de um valor de limite positivo.
- A série aritmética só permite valores de razão e limite de mesmo sinal. O caso contrário não geraria uma resposta de acordo com o que foi pedido.

# Exercício 2

Para executar o programa: `python3 exercicio_2.py`

# Exercício 3

Para executar o programa: `python3 exercicio_3.py`

# Exercício 4

Para executar o programa: `python3 exercicio_4.y`

