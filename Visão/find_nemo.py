import cv2
import numpy as np

def find_nemo():

    # Defines the range of orange that is going to be filtered.
    lower_hsv_bound = np.array([5, 135, 150])
    upper_hsv_bound = np.array([50, 255, 255])

    # Opens the video file.
    original_video = cv2.VideoCapture('Nemo.mp4')
    if original_video.isOpened() == False:
        print("Error opening the video file. Please try again with the appropriate file.")
        return -1
    
    while original_video.isOpened():

        # Reads each frame and stores it in frame.
        ret, frame = original_video.read()

        if ret == True:

            # Resizes the video, .
            frame = cv2.resize(frame, (640, 480))

            # Converts it's color space from BGR to HSV. 
            frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

            # Apply the filter to select the orange colors.
            mask = cv2.inRange(frameHSV, lower_hsv_bound, upper_hsv_bound)

            # Shows the two outputs.
            cv2.imshow('Original video', frame)
            cv2.imshow('Filtered video', mask)
            cv2.waitKey(25)

        # It will get out of the loop when it can't read the file anymore.
        else:
            break

    # Release the video and close the opened windows.
    original_video.release()
    cv2.destroyAllWindows()

find_nemo()
