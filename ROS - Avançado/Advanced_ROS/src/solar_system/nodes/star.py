#!/usr/bin/env python3

import rospy
import tf2_ros
import tf2_msgs.msg
import tf
import geometry_msgs.msg
import math

def handle_planet_pose (x, radius, planet_name, parent_name):

    br = tf2_ros.TransformBroadcaster ()
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = parent_name
    t.child_frame_id = planet_name

    t.transform.translation.x = radius * math.sin (x)
    t.transform.translation.y = radius * math.cos (x)
    t.transform.translation.z = 0.0

    t.transform.rotation.x = 0.0 
    t.transform.rotation.y = 0.0
    t.transform.rotation.z = 0.0
    t.transform.rotation.w = 1.0

    br.sendTransform (t)
    rate.sleep ()

if __name__ == '__main__':
    rospy.init_node ('star_position', anonymous = True)
    rate = rospy.Rate (100.0)
    planet_info = rospy.get_param ("/planet_info")
    
    while not rospy.is_shutdown ():
        x = rospy.Time.now ().to_sec () * math.pi

        for planet in planet_info:
            handle_planet_pose ((x / planet ["time_coefficient"]), planet ["radius"], planet ["name"], planet ["parent"])
